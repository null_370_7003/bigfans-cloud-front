/**
 * Created by lichong on 2/23/2018.
 */
import React, {Component} from 'react';
import {Card , Row} from 'antd';
import HttpUtils from "../../../utils/HttpUtils";

const gridStyle = {
    width: '20%',
    height:'250px',
    textAlign: 'center',
};

class Floor extends Component {

    state = {
        floors: []
    }

    componentDidMount() {
        var self = this;
        HttpUtils.getFloor({}, {
            success: function (response) {
                self.setState({floors: response.data});
            }
        });
    }


    render() {
        return (
            <div>
                {
                    this.state.floors.map((floor, index) => {
                        return (
                            <Card
                                key={index}
                                title={floor.title}
                                bordered={false}
                                extra={<a href={'/search?catId=' + floor.categoryId}>更多</a>}
                            >
                                {
                                    floor.productList.map((prod, index) => {
                                        return (
                                            <Card.Grid style={gridStyle} key={index}>
                                                <Row>
                                                    <a href={'/product/' + prod.id} target='_blank'>
                                                        <img style={{width: '150px' , height:'150px'}}
                                                             src={prod.imagePath}/>
                                                    </a>
                                                </Row>
                                                <Row>
                                                    <a href={'/product/' + prod.id} target='_blank'>{prod.name}</a><br/>
                                                    <span>{prod.price}</span>
                                                </Row>
                                            </Card.Grid>
                                        )
                                    })
                                }
                            </Card>
                        )
                    })
                }
            </div>
        );
    }
}

export default Floor;
