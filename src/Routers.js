import React from 'react';
import App from './App';
import ProductDetail from './pages/product/Detail';
import ProductSearchResult from './pages/product/SearchResult';
import Cart from './pages/cart/Cart';
import Checkout from './pages/order/Checkout'
import Pay from './pages/cashier/Pay'
import UserCenter from './pages/user/Center'
import MyOrders from './pages/user/MyOrders'
import MyAddress from './pages/user/MyAddress'
import MyCoupons from './pages/user/MyCoupons'
import OrderDetail from './pages/user/OrderDetail'
import CreateComment from './pages/comment/Create'
import CouponList from './pages/coupon/List'
import Login from './pages/passport/Login';
import Register from './pages/passport/Register';
import {Route, BrowserRouter, Switch} from 'react-router-dom';
import './index.css';

const routers =
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={App}></Route>
            <Route path="/login" component={Login}></Route>
            <Route path="/register" component={Register}></Route>
            <Route path="/product/:id" component={ProductDetail}></Route>
            <Route path="/search" component={ProductSearchResult}></Route>
            <Route path="/cart" component={Cart}></Route>
            <Route path="/checkout" component={Checkout}></Route>
            <Route path="/center" component={UserCenter}></Route>
            <Route path="/myorders" component={MyOrders}></Route>
            <Route path="/myaddress" component={MyAddress}></Route>
            <Route path="/mycoupons" component={MyCoupons}></Route>
            <Route path="/comment" component={CreateComment}/>
            <Route path="/orders/:id" component={OrderDetail}></Route>
            <Route path="/coupons" component={CouponList}/>
            <Route path="/pay" component={Pay}/>
        </Switch>
    </BrowserRouter>

export default routers;