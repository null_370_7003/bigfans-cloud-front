import React, { Component } from 'react';
import { Layout , Row , Col ,Icon , Menu , Input , Carousel , Card} from 'antd';
import { Divider } from 'antd';

import {Link} from 'react-router-dom'


class TopMenu extends Component {
  render() {
    return (
      <div>
          <Row>
              <Menu
                  mode="horizontal"
              >
                  <Menu.Item key="home">
                      <Link to="/"><Icon type="mail" />首页</Link>
                  </Menu.Item>
                  <Menu.Item key="pmt">
                      <Link to="/promotions"><Icon type="mail" />促销商品</Link>
                  </Menu.Item>
                  <Menu.Item key="coupon">
                      <Link to="/coupons"><Icon type="mail" />优惠券</Link>
                  </Menu.Item>
                  <Menu.Item key="seckill">
                      <Link to="/seckill"><Icon type="mail" />秒杀商品</Link>
                  </Menu.Item>
              </Menu>
          </Row>
          <Divider style={{ margin: '0'}}/>
      </div>
    );
  }
}

export default TopMenu;
